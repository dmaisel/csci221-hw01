# Makefile for TempConverter
permutationprogram: main.o encrypt.o decrypt.o
	g++ -Wall -ansi -g -O2 -o permutationprogram main.o encrypt.o decrypt.o
encrypt.o: encrypt.cpp encrypt.h
	g++ -Wall -ansi -g -O2 -c encrypt.cpp
decrypt.o: decrypt.cpp decrypt.h
	g++ -Wall -ansi -g -O2 -c decrypt.cpp
main.o: main.cpp encrypt.h decrypt.h
	g++ -Wall -ansi -g -O2 -c main.cpp
clean:
	@rm -f *.o permutationprogram
