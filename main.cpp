# include <iostream>
# include <string>
# include <unistd.h>
# include <fstream>
# include "encrypt.h"
# include "decrypt.h"
using namespace std;

int main(int argc, char** argv){
	char opt;
	int eflag, dflag, iflag, oflag;
	int blockSize = 0;
	string cipherText = "";
	string inputFile = "";
    string outputFile;
    int ascii;
    

	eflag = 0;
	dflag = 0;
	iflag = 0;
	oflag = 0;
    
    Encrypt en;

	while ( (opt = getopt(argc, argv, "i:o:ed")) != -1){
		switch(opt){
		case 'i':
			iflag = 1;
			inputFile = optarg;
			break;
		case 'o':
			oflag = 1;
			outputFile = optarg;
			break;
		case 'e':
			eflag = 1;
			break;
		case 'd':
			dflag = 1;
			break;
		default:
			cout << "Unknown option" << argv[0] << endl;
		}

	}

	if(argc !=6 || (iflag == 0) || (oflag == 0) || (eflag == dflag)){
		cout << "ERROR!" << endl;
	}

	fstream cipherFile (inputFile.c_str(), ios::in);
    
	if (cipherFile.is_open()){
			while (getline(cipherFile, cipherText)){
//				cout << cipherText << endl;
				}
			cipherFile.close();
			}
			else
				cout << "Unable to open file" << endl;
                
    cout << "Welcome to the Permutation Cipher" << endl;
    if(eflag == 1)
        {
            cout << "Selected Mode: Encrypt" << endl;
            }
            
    else if(dflag == 1)
        {
            cout << "Selected Mode: Decrypt" << endl;
            }
    else
        {
            cout << "You have not selected a mode, plese try again" << endl;
            }
    cout << "Input file: " << inputFile << endl;
    cout << "Output file: " << outputFile << endl;
    cout << "Please enter block size:";
    cin >> blockSize;
    int permNums[blockSize];
    cout << "enter the permutation separated by enter(e.g., 2, enter, 1, enter, 4, enter, 3, enter):" << endl;
    for (int i = 0; i < blockSize; i++)
        {
            cin >> permNums[i];
        }
    if(eflag == 1)
        {   
            string enText = en.permEncrypt(cipherText, blockSize, permNums);
               
            cout << "Encrypted Text: " << enText << endl;
               
            char t[enText.length()];
            for (int i = 0; i <= enText.length(); i++){
                ascii = enText[i];  
                t[i] = ascii;
                  }
            
            fstream binaryFile (outputFile.c_str(), ios::out | ios::binary);
            if (binaryFile.is_open()){
                binaryFile.write(t, enText.length());
                binaryFile.close();
                }
                else
                cout << "Unable to open file";
        }
            
    if(dflag == 1)
        {
//            cout << "D Selected!" << endl;
            
            Decrypt de;
            string destr;
            destr = de.permDecrypt(cipherText, blockSize, permNums);
            cout << "Decrypted Text: " << destr << endl;
            
            fstream deFile (outputFile.c_str(), ios::out);
            if (deFile.is_open()){
                deFile << destr;
                deFile.close();
                }
            else
            cout << "Unable to open file";
            
            }


	return 0;
}

