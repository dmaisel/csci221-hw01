#ifndef ENCRYPT_H
#define ENCRYPT_H

#include <string>

class Encrypt
{
public:
   
	   std::string permEncrypt(std::string s, int block, int perm[]);
       
};
#endif
